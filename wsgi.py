from flask import Flask
application = Flask(__name__)

@application.route("/")
def hello():
    return "Hello World!"

def say_hello_simple(name, ntimes, event):
	""" String interpolation -> https://pyformat.info/ """

	template = "Hello %s! This is your %dth time attending %s." 
	
	# No keyword arguments required
	template_filled = template % (
		name,
		ntimes,
		event
	)
	
	return template_filled




if __name__ == "__main__":
    application.run()
    say_hello_simple("Uche", 4, "DC")